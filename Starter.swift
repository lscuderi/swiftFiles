import UIKit

/* ##############################################
 Global functions, extensions and variables
 ############################################## */
var gradientColorTop = #colorLiteral(red: 0.207156837, green: 0.2075616419, blue: 0.2795711458, alpha: 1)
var gradientColorBottom = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1803921569, alpha: 1)

/*
 applyGradient() can be use with both gradientColorTop and gradientColorBottom variables,
 so it's easier when a gradient should be applied more than once with the same colors
 */

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    } // END - func applyGradient(colours: [UIColor])
    
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    } // END - func applyGradient(colours: [UIColor], locations: [NSNumber]?)
    
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    } // END - func roundCorners(_ corners:UIRectCorner, radius: CGFloat)
} // END - extension UIView



extension CALayer {
    /* Shadow like Sketch - https://stackoverflow.com/questions/34269399/how-to-control-shadow-spread-and-blur */
    func applySketchShadow(color: UIColor = .black, alpha: Float = 0.5, x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    } // END - func applySketchShadow
    
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            //For Center Line
            border.frame = CGRect(x: self.frame.width/2 - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        }
        
        border.backgroundColor = color.cgColor
        
        self.addSublayer(border)
    } // END - func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat)
} // END - extension CALayer



func localeCurrency(amount: NSNumber, currencyCode: Bool) -> String {
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    
    if currencyCode {
        currencyFormatter.numberStyle = NumberFormatter.Style.currency // Format in local currency for example: CHF xx'xxx.xx or USD xx,xxx.xx
    } else {
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal // Format in local currency for example: xx'xxx.xx or xx,xxx.xx
    }
    
    currencyFormatter.maximumFractionDigits = 2
    currencyFormatter.minimumFractionDigits = 2
    
    let priceString = currencyFormatter.string(from: amount)
    
    return priceString!
} // END - localeCurrency(amount: NSNumber, currencyCode: Bool)



func getCurrentDate(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy" // "18/02/1999"
    let currentDateString: String = dateFormatter.string(from: date)
    
    return currentDateString
} // END - getCurrentDate(date: Date)



func convertDateInText(date: String, onlyDay: Bool) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy" // Current date format "18/02/1999"
    let date = dateFormatter.date(from: date)
    
    if onlyDay {
        dateFormatter.dateFormat = "EEEE" // New date format "Thursday"
    } else {
        dateFormatter.dateFormat = "EEEE, MMMM dd" // New date format "Thursday, February 18th, 1999"
    }
    
    let timeStamp = dateFormatter.string(from: date!)
    
    return timeStamp
} // END - convertDateFormatter(date: String)
